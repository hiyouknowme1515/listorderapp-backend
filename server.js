//Requiring dependencies
const express = require('express');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const expressValidator = require('express-validator')
const cors = require('cors')
const fs = require('fs')

//declared routes 
const authRoute = require('./routes/auth');
const userRoute = require('./routes/user');
const orderRoute = require('./routes/order')


const app = express();
dotenv.config();

//middlewares
app.use(morgan('tiny'))
app.use(bodyParser.json())
app.use(expressValidator())
app.use(cookieParser())
app.use(cors())

//use Routes
app.use('/', authRoute)
app.use('/', userRoute)
app.use('/', orderRoute)

//to handle Unauthorized error
app.use(function (err, req, res, next) {
    if (err.name == 'UnauthorizedError') {
      res.status(401).json({
          error: "Unauthorized User"
      });
    }
  
  });

// to show the apidocs on '/' path
app.get('/', (req, res) => {
    fs.readFile('docs/api.json', (err, data) => {
        if (err) {
            res.status(400).json({
                error: err
            });
        }
        const docs = JSON.parse(data);
        res.json(docs);
    });
});




//DB connection
mongoose.connect(process.env.MONGODB_URI, {
            useNewUrlParser: true, useUnifiedTopology: true
        })
        .then(() => console.log('DB connected'));

mongoose.connection.on('error', err => {
    console.log(`DB connection error: ${err.message}`)
});


//Configured port
const port = process.env.PORT || 8080;

//Server listening 
app.listen(port, () => {
    console.log(`Server started at http://localhost:${port}`)
});


