# OrderList App Api

Technology Used: NodeJs, Express

Database Used: MongoDB,

Specifications:

1. User can create his/her account and list their orders
2. They can update and delete their account
3. Update their order, delete their order
4. On User Profile page their orders which they have posted are also shown

Routes

&quot;/register&quot;:&quot; post request to register a user&quot;,

&quot;/login&quot;:&quot; post request to login a user&quot;,

&quot;/user/:userId&quot;:&quot; get request to fetch the user whose userId is in Url , put request to update user details , delete request to delete the user&quot;,

&quot;/order/new/:userId&quot;:&quot;post request to create new order from a logged in user&quot;,

&quot;/order/:orderId&quot;:&quot;put request to update the order detalis , delete request to delete the order, get request to fetch single order&quot;,

&quot;/orders/by/:userId&quot;:&quot;get request to show the orders by a user&quot;


Folder structure

+---controllers

+---docs

+---models

+---routes

\---validators

Controllers:- It includes functions which control different operation of user , order and authentication.

Docs:- It simply has api docs documented in it

Routes:- It contains different user routes

Validators: It containes validation of create order and register user
