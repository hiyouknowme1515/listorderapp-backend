//User Register Validator
const userRegisterValidator = (req, res, next) => {
    //checking name is not null and between 3-25 characters
    req.check('name', "Name is required ").notEmpty()
    req.check('name', "Name must be from 3 to 25 character ").isLength({
        min: 3,
        max: 25
    })

    //checking email is not null, valid and normailized 
    req.check('email', 'Email must be between 3 to 32 characters')
        .matches(/.+\@.+\..+/)
        .withMessage('Email must contain @')
        .isLength({
            min: 4,
            max: 2000
        });
    
    //checking for password 
    req.check('hashed_password', 'Password is required').notEmpty()
    req.check('hashed_password')
            .isLength({ min: 6})
            .withMessage('Password must contain at least 6 characters')
            .matches(/\d/)
            .withMessage('Password must contain a number')
    
    //check for any errors
    const errors = req.validationErrors()
    
    //if find any error it shows the first one 
    if(errors){
        const firstError = errors.map(error => error.msg)[0];
        return res.status(400).json({ error: firstError})
    }

    //proceed to next middleware
    next()
}

// Create Order validation 
createOrderValidator = (req, res, next ) => {
    //for the Order Name
    req.check('orderName',"Write an Order Name").notEmpty()
    req.check('orderName', "Event Name must be from 2 to 150 characters ").isLength({
        min: 2,
        max: 150
    })

    //for the Order Quantity
    req.check('orderQuantity',"Order Quantity must be a number").isNumeric()
    

    // for the Date of Order 
    req.check('orderDate',"Date must not be empty").notEmpty()
    

    //for the Order Status
    req.check('orderStatus', "Order status must be required").notEmpty()

    //check for any errors
    const errors = req.validationErrors();

    //show the first error when there is multiple errrors
    if(errors){
        const firstError = errors.map(error => error.msg)[0]
        return res.status(400).json({
            error: firstError
        })
    }
    //proceed to next middleware
    next()
}

module.exports = {
    userRegisterValidator,
    createOrderValidator
}