//Dependencies Required
const  _= require('lodash')

//Some created modules required
const { Order }= require('../models/order')

// to add order when orderId is in URL
const orderById = (req, res, next, id) => {
   
    Order.findById(id)
        .populate('postedBy', '_id name')
        .select('_id orderName orderQuantity orderDate orderStatus updated')
        .exec((err, order) => {
            if(err || !order ){
                return res.status(400).json({
                    error: err
                })
            }
            req.order = order
           
            next()
        })
}


// to create an Order 
const createOrder = async(req, res) => {
    
    const order = await new Order(req.body)

    req.profile.hashed_password = undefined
    order.postedBy = req.profile
    order.save().then(result => {
        res.status(200).json({
            order : result
        });
    });
};

// to show all the orders by an user
const ordersByUser = (req, res) => {
    Order.find({ postedBy: req.profile._id })
        .populate('postedBy', '_id name')
        .select('_id orderName orderQuantity orderDate orderStatus updated')
        .sort('_created')
        .exec((err, orders) => {
            if(err){
                return res.status(400).json({
                    error: err
                })
               
            }
            res.json(orders)
        })
}

// to verify the user which created that order is the logged in user
const isPlacedOrder = (req, res, next ) => {
    let sameUser = req.order && req.auth && req.order.postedBy._id == req.auth._id
   
    if(!sameUser){
        return res.status(403).json({
            error: 'User is not authorized'
        })
    }
    next()
}

// to update an order
const updateOrder = (req, res, next ) => {
    let order = req.order
    order = _.extend(order , req.body)
    order.updated = Date.now()
    order.save(err => {
        if(err){
            return res.status(400).json({
                error: err
            })
        }
        res.json(order)
    })
}

// to delete an order 
const deleteOrder = (req, res) => {
    let order = req.order
    order.remove((err, order) => {
        if(err){
            return res.status(400).json({
                error: err
            })
        }
        res.json({
            message: "Event deleted successfully"
        })
    })
}

//to display single order 
const singleOrder = (req, res) => {
    return res.json(req.order);
};



module.exports = {
    
    createOrder,
    deleteOrder,
    orderById,
    updateOrder,
    isPlacedOrder,
    ordersByUser,
    singleOrder
}