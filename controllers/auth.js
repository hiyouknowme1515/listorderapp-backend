//Dependencies required 
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');
const dotenv = require('dotenv');

//Require some created modules
const { User } = require('../models/user');

dotenv.config()
//register User controller
const registerUser = async(req, res) => {
    //Checking if the user already exists
    const userExits = await User.findOne({ email: req.body.email});
    if(userExits){
        return res.status(403).json({
            error: "User already exists with that email Please Login"
        });
    };

    //Registering User now
    const body = req.body;
    if(!(body.email && body.hashed_password && body.name))
        return res.status(400).send({ error: "Data is not formatted properly "});

    const user = await new User(body);

    const salt = await bcrypt.genSalt(12);
    user.hashed_password = await bcrypt.hash(user.hashed_password, salt);
    user.save()
        .then(() => res.status(201).json({
            message: "User successfully registered! Please LogIn "
        }))
        .catch((err) => res.json({
            error: err
        }))
};

//login user controller 
const loginUser = async(req, res) => {

    const body = req.body;
    const user = await User.findOne({ email: body.email });
    if (user) {
      // check user password with hashed password stored in the database
      const validPassword = await bcrypt.compare(body.hashed_password, user.hashed_password);

      if (validPassword) {
        //generate a token with userid and secret
        const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET)

        //persist the token as 'tkn' in cookie with expiry date 
        res.cookie("tkn", token , { expire: new Date() +9999 })

        //return response with user and token to frontend client 
        const{ _id, name , email } = user

        res.status(200).json({ token, user: { _id, email, name } });
      } else {
          // password is incorrect
        res.status(400).json({ error: "Password is incorrect! Please enter the correct password" });
      }
    } else {
        //user didnt have any account 
      res.status(401).json({ error: "User does not exist! Please register" });
    }
}

//for logging out the user 
const logoutUser = (req, res ) => {
    res.clearCookie('tkn');
    res.json({
        message: "Logout Successful "
    })
}

//for protecting certain routes

const requireLogin = expressJwt({
    secret: process.env.JWT_SECRET,
    userProperty: 'auth',
    algorithms: ['HS256'],
    
})

module.exports = {
    registerUser,
    loginUser,
    logoutUser,
    requireLogin
}

