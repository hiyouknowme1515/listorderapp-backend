//Dependecies required
const mongoose = require('mongoose')

const { Schema } = mongoose
const { ObjectId } = mongoose.Schema

//Order Schema Declared
const OrderSchema = new Schema({
    orderName: {
        type: String,
        trim: true,
        required: true
    },
    orderQuantity: {
        type: Number,
        required: true
    },
    orderDate: {
        type: Date,
        default: Date.now
    },
    orderStatus: {
        type: String,
        trim: true,
        required: true
    },
    postedBy: {
        type: ObjectId,
        ref: 'User'
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date,
        default: Date.now
    }
});

const Order = mongoose.model("Order", OrderSchema);

module.exports = {
    Order
}