//Dependicies Requiered
const express = require('express')

//Different created modules requiered
const {userById} = require('../controllers/user')
const { 
    createOrder,
    isPlacedOrder,
    updateOrder,
    orderById,
    deleteOrder,
    ordersByUser,
    singleOrder
} = require('../controllers/order')
const { createOrderValidator } = require('../validators')
const { requireLogin } = require('../controllers/auth')

// Initialized express Router
const router = express.Router()


//to create new order by a loggedin user
router.post('/order/new/:userId',requireLogin, createOrderValidator, createOrder)

//to update an order
router.put('/order/:orderId', requireLogin, isPlacedOrder, updateOrder)

//to delete an order
router.delete('/order/:orderId', requireLogin, isPlacedOrder, deleteOrder)

//to show all the orders by a user
router.get('/orders/by/:userId', requireLogin, ordersByUser)

//to call userById function when userId is in URL
router.param('userId', userById)

//to show the single order
router.get('/orders/:orderId', singleOrder)

//to call orderById function when orderId is in URL
router.param('orderId', orderById)

module.exports = router